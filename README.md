# Random GLSL Shaders

Random GLSL shaders I made through accidents or refined afterwards.
Thanks to https://thebookofshaders.com/ for nice glsl tutorials.

## [80s-pyramid.glsl](https://gitlab.com/stiblu/random-glsl-shaders/-/blob/master/src/80s-pyramid.glsl)

!["80s-pyramid.glsl" gif animation](https://i.imgur.com/kAoUAvu.mp4)

## [bavarian-memorial-shader.glsl](https://gitlab.com/stiblu/random-glsl-shaders/-/blob/master/src/bavarian-memorial-shader.glsl)

!["bavarian-memorial-shader.glsl" gif animation](https://i.imgur.com/GXa12p6.mp4)
