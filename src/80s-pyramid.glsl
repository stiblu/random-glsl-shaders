#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);
    
    if (st.x <= 0.5){
        float w = step(st.x * abs(sin((u_time + 100.0) * 5.0 * st.y)), st.y);
        color = vec3(w,st.y,abs(sin(u_time)));
    }
    else{
        float w = step((1.0 - st.y) * abs(sin((u_time + 100.0) * 5.0 * st.y)), st.x);
        color = vec3(w,st.y,abs(sin(u_time)));
    }
    gl_FragColor = vec4(color,1.0);
}