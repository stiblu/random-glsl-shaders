#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

void main(){

    vec2 st = gl_FragCoord.xy / u_resolution.xy;
    vec3 color = vec3(0.0);

    float x = step(sin(u_time * st.y * 20.0), sin(u_time * st.x * 20.0));

    color = vec3(x,1.0, 1.0);
    gl_FragColor = vec4(color, 1.0);
}